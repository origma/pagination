/* 
 * Copyright (C) Origma Pty Ltd (ACN 629 381 184). All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ben McLean <ben@origma.com.au>, November 2019
 */
package au.com.origma.pagination;

import java.util.List;

public class OffsetPage<T, ID_TYPE> extends Page<T, ID_TYPE> {

	long offset;

	public OffsetPage() {
		super();
	}

	public OffsetPage(int count, Integer limit, List<T> contents, long offset) {
		super(count, limit, contents);
		this.offset = offset;
	}

	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}
	
}
