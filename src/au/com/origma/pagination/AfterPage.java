package au.com.origma.pagination;

import java.util.List;

public class AfterPage<T, ID_TYPE> extends Page<T, ID_TYPE> {

	ID_TYPE start;
	ID_TYPE next;
	
	public AfterPage(){}
	
	public AfterPage(ID_TYPE start, ID_TYPE next, int count, Integer limit, List<T> contents) {
		super();
		this.start = start;
		this.next = next;
		this.count = count;
		this.limit = limit;
		this.contents = contents;
	}

	public ID_TYPE getStart() {
		return start;
	}

	public void setStart(ID_TYPE start) {
		this.start = start;
	}

	public ID_TYPE getNext() {
		return next;
	}

	public void setNext(ID_TYPE next) {
		this.next = next;
	}
	
}
