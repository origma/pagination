/* 
 * Copyright (C) Origma Pty Ltd (ACN 629 381 184). All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ben McLean <ben@origma.com.au>, November 2019
 */
package au.com.origma.pagination;

import java.util.List;

public abstract class Page<T, ID_TYPE> {

	int count;
	Integer limit;
	List<T> contents;
	
	public Page(){}
	
	public Page(int count, Integer limit, List<T> contents) {
		super();
		this.count = count;
		this.limit = limit;
		this.contents = contents;
	}

	public int getCount() {
		return count;
	}
	
	public void setCount(int count) {
		this.count = count;
	}
	
	public Integer getLimit() {
		return limit;
	}
	
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	
	public List<T> getContents() {
		return contents;
	}
	
	public void setContents(List<T> contents) {
		this.contents = contents;
	}
	
}
